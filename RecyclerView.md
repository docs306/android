# RecyclerView

[文档](https://developer.android.google.cn/reference/kotlin/androidx/recyclerview/widget/RecyclerView)
RecyclerView 可以轻松高效地显示大量数据。
并可以定义每个列表项的外观，RecyclerView 会根据需要动态回收/创建元素。
当列表项滚动出屏幕时，RecyclerView 不会销毁其视图，而是会对屏幕上滚动的新列表项重用该视图。
这种重用可以显著提高性能，改善应用响应能力并降低功耗。

## 关键类
构建动态列表，需要将多个不同的类搭配使用。

### RecyclerView.Adapter

- 可以通过扩展 RecyclerView.Adapter 来自定义 Adapter，RecyclerView 会请求这些视图，并通过在 Adapter 中调用方法，将视图绑定到其数据。

- 列表中的每个独立元素都由一个 ViewHolder 对象进行定义。创建 ViewHolder 时，它并没有任何关联的数据。创建 ViewHolder 后，RecyclerView 会将其绑定到其数据。您可以通过扩展 RecyclerView.ViewHolder 来定义 ViewHolder。

实现 Adapter 和 ViewHolder
确定布局后，您需要实现 Adapter 和 ViewHolder。这两个类配合使用，共同定义数据的显示方式。ViewHolder 是包含列表中各列表项的布局的 View 的封装容器。Adapter 会根据需要创建 ViewHolder 对象，还会为这些视图设置数据。将视图与其数据相关联的过程称为“绑定”。

定义 Adapter 时，您需要替换三个关键方法：

- onCreateViewHolder()：每当 RecyclerView 需要创建新的 ViewHolder 时，它都会调用此方法。此方法会创建并初始化 ViewHolder 及其关联的 View，但不会填充视图的内容，因为 ViewHolder 此时尚未绑定到具体数据。

- onBindViewHolder()：RecyclerView 调用此方法将 ViewHolder 与数据相关联。此方法会提取适当的数据，并使用该数据填充 ViewHolder 的布局。例如，如果 RecyclerView 显示的是一个名称列表，该方法可能会在列表中查找适当的名称，并填充 ViewHolder 的 TextView widget。

- getItemCount()：RecyclerView 调用此方法来获取数据集的大小。例如，在通讯簿应用中，这可能是地址总数。RecyclerView 使用此方法来确定什么时候没有更多的列表项可以显示。

### LayoutManager
布局管理器负责排列列表中的各个元素，也可以定义自己的布局管理器。
RecyclerView 库提供了三种布局管理器，用于处理最常见的布局情况：

LinearLayoutManager 将各个项排列在一维列表中。
- mLayoutManager = new LinearLayoutManager(getActivity());

GridLayoutManager 将所有项排列在二维网格中：
- 如果网格垂直排列，GridLayoutManager 会尽量使每行中所有元素的宽度和高度相同，但不同的行可以有不同的高度。
- 如果网格水平排列，GridLayoutManager 会尽量使每列中所有元素的宽度和高度相同，但不同的列可以有不同的宽度。
- mLayoutManager = new GridLayoutManager(getActivity(), 2);

StaggeredGridLayoutManager 与 GridLayoutManager 类似，但不要求同一行中的列表项具有相同的高度（垂直网格有此要求）或同一列中的列表项具有相同的宽度（水平网格有此要求）。其结果是，同一行或同一列中的列表项可能会错落不齐。

recyclerView.setLayoutManager(mLayoutManager);


### ItemDecoration

recyclerView.addItemDecoration(did)

### ItemAnimator
每当数据发生变化时，RecyclerView 会通过实现RecyclerView.ItemAnimator 数据项的动画。

recyclerView.itemAnimator = dia


### 其他方法
recyclerView.scrollToPosition(scrollPosition); // 滑动到指定索引位置

借助 recyclerview-selection 库，用户可以通过触摸或鼠标输入来选择 RecyclerView 列表中的项


```
class DemoActivity: AppCompatActivity() {
    private lateinit var itemList: ArrayList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_demo)

        itemList = ArrayList()
        for (i in 0..30) {
            itemList.add("测试数据：${i}")
        }
        initView()
    }
    
    fun initView() {
        val recyclerView = findViewById<RecyclerView>(R.id.demo_recycler)

//        val lm = LinearLayoutManager(this)
//        val lm = GridLayoutManager(this, 2)
        val lm = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        lm.gapStrategy = StaggeredGridLayoutManager.GAP_HANDLING_NONE
        val did = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        val dia = DefaultItemAnimator()
        dia.addDuration = 1000
        dia.removeDuration = 1000
        recyclerView.addItemDecoration(did)
        recyclerView.itemAnimator = dia
        recyclerView.layoutManager = lm
        recyclerView.adapter = RecyclerAdapter1(this, itemList)
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
//                lm.invalidateSpanAssignments()
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
            }
        })

    }
}

```

```
class RecyclerAdapter1(val context: Context, val list: ArrayList<String>): RecyclerView.Adapter<RecyclerAdapter1.DemoViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DemoViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.ada_recycler, parent, false)
        val holder = DemoViewHolder(view)
        return holder
    }

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onBindViewHolder(holder: DemoViewHolder, position: Int) {

        holder.textView.text = list.get(position)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class DemoViewHolder(view: View): RecyclerView.ViewHolder(view) {
        var textView: TextView
        init {
            textView = view.findViewById(R.id.ada_demo_text)
        }
    }
}
```
        
